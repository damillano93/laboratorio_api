// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de sede
var sede_controller = require('../controllers/sede');

// GET /:id
router.get('/:id', sede_controller.sede_details);
// GET /
router.get('/', sede_controller.sede_all);
// POST /
router.post('/', sede_controller.sede_create);
// PUT /:id
router.put('/:id', sede_controller.sede_update);
// DELETE /:id
router.delete('/:id', sede_controller.sede_delete);

//export router
module.exports = router;