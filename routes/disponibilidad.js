// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de disponibilidad
var disponibilidad_controller = require('../controllers/disponibilidad');

// GET /:id
router.get('/:id', disponibilidad_controller.disponibilidad_details);
// GET /
router.get('/', disponibilidad_controller.disponibilidad_all);
// POST /
router.post('/', disponibilidad_controller.disponibilidad_create);
// PUT /:id
router.put('/:id', disponibilidad_controller.disponibilidad_update);
// DELETE /:id
router.delete('/:id', disponibilidad_controller.disponibilidad_delete);
// PUT laboratorio 
router.put('/laboratorio/:id',disponibilidad_controller.Disponibilidad_add_laboratorio);

//export router
module.exports = router;