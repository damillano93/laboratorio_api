// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de nombre_dia
var nombre_dia_controller = require('../controllers/nombre_dia');

// GET /:id
router.get('/:id', nombre_dia_controller.nombre_dia_details);
// GET /
router.get('/', nombre_dia_controller.nombre_dia_all);
// POST /
router.post('/', nombre_dia_controller.nombre_dia_create);
// PUT /:id
router.put('/:id', nombre_dia_controller.nombre_dia_update);
// DELETE /:id
router.delete('/:id', nombre_dia_controller.nombre_dia_delete);

//export router
module.exports = router;