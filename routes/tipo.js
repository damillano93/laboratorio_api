// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de tipo
var tipo_controller = require('../controllers/tipo');

// GET /:id
router.get('/:id', tipo_controller.tipo_details);
// GET /
router.get('/', tipo_controller.tipo_all);
// POST /
router.post('/', tipo_controller.tipo_create);
// PUT /:id
router.put('/:id', tipo_controller.tipo_update);
// DELETE /:id
router.delete('/:id', tipo_controller.tipo_delete);

//export router
module.exports = router;