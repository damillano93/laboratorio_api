// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de ubicacion
var ubicacion_controller = require('../controllers/ubicacion');

// GET /:id
router.get('/:id', ubicacion_controller.ubicacion_details);
// GET /
router.get('/', ubicacion_controller.ubicacion_all);
// POST /
router.post('/', ubicacion_controller.ubicacion_create);
// PUT /:id
router.put('/:id', ubicacion_controller.ubicacion_update);
// DELETE /:id
router.delete('/:id', ubicacion_controller.ubicacion_delete);

//export router
module.exports = router;