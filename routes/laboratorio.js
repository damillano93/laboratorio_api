// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de laboratorio
var laboratorio_controller = require('../controllers/laboratorio');

// GET /:id
router.get('/:id', laboratorio_controller.laboratorio_details);
// GET /
router.get('/', laboratorio_controller.laboratorio_all);
// POST /
router.post('/', laboratorio_controller.laboratorio_create);
// PUT /:id
router.put('/:id', laboratorio_controller.laboratorio_update);
// DELETE /:id
router.delete('/:id', laboratorio_controller.laboratorio_delete);
// PUT sede 
router.put('/sede/:id',laboratorio_controller.Laboratorio_add_sede);

//export router
module.exports = router;