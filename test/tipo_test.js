var superagent = require('superagent')
var expect = require('expect.js')

describe('Test tipo', function () {
  var id
  var url = 'http://localhost:3001';
  // GET /


  // ------------------Pruebas a la ruta tipo------------------

  // POST /tipo
  it('Test POST /tipo ', function (done) {
    superagent.post(url + '/tipo')
      .send({
        nombre: "Electronica" , 
        descripcion:"Laboratorio de tipo electronica" 
      })
      .end(function (e, res) {
        id = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  // GET /tipo
  it('Test GET /tipo', function (done) {
    superagent.get(url + '/tipo')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        done()
      })
  })


 // GET /tipo/:id
 it('test GET /tipo/:id', function (done) {
  superagent.get(url + '/tipo/'+id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body._id).to.eql(id)
      done()
    })
})
// PUT /tipo/:id
it('Test PUT /tipo/:id ', function (done) {
  superagent.put(url + '/tipo/'+id)
    .send({
      nombre: "Circuitos" , 
      descripcion:"Laboratorio de tipo electronica" 
    })
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(res.body.status).to.eql('updated')
      expect(res.body.tipo.nombre).to.eql('Circuitos')
      done()
    })
})

// DELETE /tipo/:id
it('Test DELETE /tipo/:id ', function (done) {
  superagent.delete(url +'/tipo/' + id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body.status).to.eql('deleted')
      done()
    })
})

})
