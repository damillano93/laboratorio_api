var superagent = require('superagent')
var expect = require('expect.js')

describe('Test sede', function () {
  var id
  var url = 'http://localhost:3001';


  // ------------------Pruebas a la ruta sede------------------

  // POST /sede
  it('Test POST /sede ', function (done) {
    superagent.post(url + '/sede')
      .send({
        nombre: "Ingenieria" , 
        descripcion:"Facultad de ingenieria" , 
        direccion: "calle 40"
      })
      .end(function (e, res) {
        id = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  // GET /sede
  it('Test GET /sede', function (done) {
    superagent.get(url + '/sede')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        done()
      })
  })


 // GET /sede/:id
 it('test GET /sede/:id', function (done) {
  superagent.get(url + '/sede/'+id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body._id).to.eql(id)
      done()
    })
})
// PUT /sede/:id
it('Test PUT /sede/:id ', function (done) {
  superagent.put(url + '/sede/'+id)
    .send({
      nombre: "Ciencias" , 
      descripcion:"Facultad de ingenieria" , 
      direccion: "calle 40"
    })
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(res.body.status).to.eql('updated')
      expect(res.body.sede.nombre).to.eql('Ciencias')
      done()
    })
})

// DELETE /sede/:id
it('Test DELETE /sede/:id ', function (done) {
  superagent.delete(url +'/sede/' + id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body.status).to.eql('deleted')
      done()
    })
})

})
