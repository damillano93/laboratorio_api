//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Tipo
var TipoSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 

});

TipoSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Tipo', TipoSchema);