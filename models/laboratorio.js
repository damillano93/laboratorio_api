//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Laboratorio
var LaboratorioSchema = new Schema({
nombre: {type:String, required:true}, 
estado: {type:String, required:true}, 
tipo: {type:Schema.Types.ObjectId, ref: 'Tipo' , autopopulate: true }, 
ubicacion: {type:Schema.Types.ObjectId, ref: 'Ubicacion' , autopopulate: true }, 
sede: {type:Schema.Types.ObjectId, ref: 'Sede' , autopopulate: true }, 

});

LaboratorioSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Laboratorio', LaboratorioSchema);