//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Sede
var SedeSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 
direccion: {type:String, required:true}, 

});

SedeSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Sede', SedeSchema);