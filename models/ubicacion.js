//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Ubicacion
var UbicacionSchema = new Schema({
nombre: {type:String, required:true}, 
descripcion: {type:String, required:true}, 
direccion: {type:String, required:true}, 

});

UbicacionSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Ubicacion', UbicacionSchema);