//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Disponibilidad
var DisponibilidadSchema = new Schema({
fecha: {type:String, required:true}, 
dia: {type:Number, required:true}, 
nombre_dia: {type:Schema.Types.ObjectId, ref: 'Nombre_dia' , autopopulate: true }, 
laboratorio: {type:Schema.Types.ObjectId, ref: 'Laboratorio' , autopopulate: true }, 
descripcion: {type:String, required:true}, 
responsable: {type:String, required:true}, 

});

DisponibilidadSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Disponibilidad', DisponibilidadSchema);