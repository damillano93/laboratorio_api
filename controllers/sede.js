//importar modelo
var Sede = require('../models/sede');

//funcion create
exports.sede_create = function (req, res) {
    var sede = new Sede(
        req.body
    );

    sede.save(function (err, sede) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: sede._id})
    })
};
//funcion read by id
exports.sede_details = function (req, res) {
    Sede.findById(req.params.id, function (err, sede) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(sede);
    })
};
//funcion read all
exports.sede_all = function (req, res) {
    Sede.find(req.params.id, function (err, sede) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(sede);
    })
};
//funcion update
exports.sede_update = function (req, res) {
    Sede.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, sede) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", sede: sede });
    });
};

//funcion delete
exports.sede_delete = function (req, res) {
    Sede.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};