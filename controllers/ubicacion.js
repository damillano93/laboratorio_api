//importar modelo
var Ubicacion = require('../models/ubicacion');

//funcion create
exports.ubicacion_create = function (req, res) {
    var ubicacion = new Ubicacion(
        req.body
    );

    ubicacion.save(function (err, ubicacion) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: ubicacion._id})
    })
};
//funcion read by id
exports.ubicacion_details = function (req, res) {
    Ubicacion.findById(req.params.id, function (err, ubicacion) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(ubicacion);
    })
};
//funcion read all
exports.ubicacion_all = function (req, res) {
    Ubicacion.find(req.params.id, function (err, ubicacion) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(ubicacion);
    })
};
//funcion update
exports.ubicacion_update = function (req, res) {
    Ubicacion.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, ubicacion) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", ubicacion: ubicacion });
    });
};

//funcion delete
exports.ubicacion_delete = function (req, res) {
    Ubicacion.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};