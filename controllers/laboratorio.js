//importar modelo
var Laboratorio = require('../models/laboratorio');

//funcion create
exports.laboratorio_create = function (req, res) {
    var laboratorio = new Laboratorio(
        req.body
    );

    laboratorio.save(function (err, laboratorio) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: laboratorio._id})
    })
};
//funcion read by id
exports.laboratorio_details = function (req, res) {
    Laboratorio.findById(req.params.id, function (err, laboratorio) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(laboratorio);
    })
};
//funcion read all
exports.laboratorio_all = function (req, res) {
    Laboratorio.find(req.params.id, function (err, laboratorio) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(laboratorio);
    })
};
//funcion update
exports.laboratorio_update = function (req, res) {
    Laboratorio.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, laboratorio) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", laboratorio: laboratorio });
    });
};
//funcion add tipo
exports.Laboratorio_add_tipo = function (req, res) {
Laboratorio.findByIdAndUpdate(req.params.id, { $push: { tipo: req.body.tipo } }, { new: true }, function (err, Laboratorio) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Laboratorio: Laboratorio });
});
};
//funcion add ubicacion
exports.Laboratorio_add_ubicacion = function (req, res) {
Laboratorio.findByIdAndUpdate(req.params.id, { $push: { ubicacion: req.body.ubicacion } }, { new: true }, function (err, Laboratorio) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Laboratorio: Laboratorio });
});
};
//funcion add sede
exports.Laboratorio_add_sede = function (req, res) {
Laboratorio.findByIdAndUpdate(req.params.id, { $push: { sede: req.body.sede } }, { new: true }, function (err, Laboratorio) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Laboratorio: Laboratorio });
});
};

//funcion delete
exports.laboratorio_delete = function (req, res) {
    Laboratorio.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};