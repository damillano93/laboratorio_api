//importar modelo
var Tipo = require('../models/tipo');

//funcion create
exports.tipo_create = function (req, res) {
    var tipo = new Tipo(
        req.body
    );

    tipo.save(function (err, tipo) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: tipo._id})
    })
};
//funcion read by id
exports.tipo_details = function (req, res) {
    Tipo.findById(req.params.id, function (err, tipo) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo);
    })
};
//funcion read all
exports.tipo_all = function (req, res) {
    Tipo.find(req.params.id, function (err, tipo) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(tipo);
    })
};
//funcion update
exports.tipo_update = function (req, res) {
    Tipo.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, tipo) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", tipo: tipo });
    });
};

//funcion delete
exports.tipo_delete = function (req, res) {
    Tipo.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};