//importar modelo
var Disponibilidad = require('../models/disponibilidad');

//funcion create
exports.disponibilidad_create = function (req, res) {
    var disponibilidad = new Disponibilidad(
        req.body
    );

    disponibilidad.save(function (err, disponibilidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: disponibilidad._id})
    })
};
//funcion read by id
exports.disponibilidad_details = function (req, res) {
    Disponibilidad.findById(req.params.id, function (err, disponibilidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(disponibilidad);
    })
};
//funcion read all
exports.disponibilidad_all = function (req, res) {
    Disponibilidad.find(req.params.id, function (err, disponibilidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(disponibilidad);
    })
};
//funcion update
exports.disponibilidad_update = function (req, res) {
    Disponibilidad.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, disponibilidad) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", disponibilidad: disponibilidad });
    });
};
//funcion add nombre_dia
exports.Disponibilidad_add_nombre_dia = function (req, res) {
Disponibilidad.findByIdAndUpdate(req.params.id, { $push: { nombre_dia: req.body.nombre_dia } }, { new: true }, function (err, Disponibilidad) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Disponibilidad: Disponibilidad });
});
};
//funcion add laboratorio
exports.Disponibilidad_add_laboratorio = function (req, res) {
Disponibilidad.findByIdAndUpdate(req.params.id, { $push: { laboratorio: req.body.laboratorio } }, { new: true }, function (err, Disponibilidad) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Disponibilidad: Disponibilidad });
});
};

//funcion delete
exports.disponibilidad_delete = function (req, res) {
    Disponibilidad.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};