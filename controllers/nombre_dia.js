//importar modelo
var Nombre_dia = require('../models/nombre_dia');

//funcion create
exports.nombre_dia_create = function (req, res) {
    var nombre_dia = new Nombre_dia(
        req.body
    );

    nombre_dia.save(function (err, nombre_dia) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
            
        }
        res.send({status: "created", id: nombre_dia._id})
    })
};
//funcion read by id
exports.nombre_dia_details = function (req, res) {
    Nombre_dia.findById(req.params.id, function (err, nombre_dia) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(nombre_dia);
    })
};
//funcion read all
exports.nombre_dia_all = function (req, res) {
    Nombre_dia.find(req.params.id, function (err, nombre_dia) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send(nombre_dia);
    })
};
//funcion update
exports.nombre_dia_update = function (req, res) {
    Nombre_dia.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, nombre_dia) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status: "updated", nombre_dia: nombre_dia });
    });
};

//funcion delete
exports.nombre_dia_delete = function (req, res) {
    Nombre_dia.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            return next(err);
        }
        res.send({status:"deleted"});
    })
};