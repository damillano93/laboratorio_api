// importar biblioteca express
var express = require('express');
// importar biblioteca body-parser
var bodyParser = require('body-parser');
// importar biblioteca morgan
var logger = require('morgan')
// importar router tipo
var tipo = require('./routes/tipo');
// importar router ubicacion
var ubicacion = require('./routes/ubicacion');
// importar router sede
var sede = require('./routes/sede');
// importar router laboratorio
var laboratorio = require('./routes/laboratorio');
// importar router disponibilidad
var disponibilidad = require('./routes/disponibilidad');
// importar router nombre_dia
var nombre_dia = require('./routes/nombre_dia');

var cors = require('cors');
// importar router de status
var status = require('./routes/status'); 
// uso biblioteca express
var app = express();
//visualizacion respuestas en consola
app.use(logger('dev'))
//agrega CORS para tener accesso desde otras aplicaciones
app.use(cors());
// uso biblioteca mongoose
var mongoose = require('mongoose');
// conexion DB 
var dev_db_url = process.env.MONGO_URL
console.log(dev_db_url);
mongoose.connect(dev_db_url);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// uso de respuestas y llamados en JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
//creacion ruta /tipo
app.use('/tipo', tipo);
//creacion ruta /ubicacion
app.use('/ubicacion', ubicacion);
//creacion ruta /sede
app.use('/sede', sede);
//creacion ruta /laboratorio
app.use('/laboratorio', laboratorio);
//creacion ruta /disponibilidad
app.use('/disponibilidad', disponibilidad);
//creacion ruta /nombre_dia
app.use('/nombre_dia', nombre_dia);

//creacion ruta /
app.use('/', status);
// puerto de servicio
var port = 3001;
// inicir la conexion
app.listen(port, () => {
    console.log('Server in port ' + port);
});